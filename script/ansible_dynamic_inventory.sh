#Download ec2.ini and ec2.py on control node

dynamic()
{
cd /etc/ansible
sudo wget https://raw.githubusercontent.com/ansible/ansible/devel/contrib/inventory/ec2.py
sudo wget https://raw.githubusercontent.com/ansible/ansible/devel/contrib/inventory/ec2.ini
sudo chmod +x /etc/ansible/ec2.py
sudo chmod +x /etc/ansible/ec2.py


export AWS_ACCESS_KEY_ID='paste_access_key'
export AWS_SECRET_ACCESS_KEY='paste_secret_key'
export EC2_INI_PATH=/etc/ansible/ec2.ini
export ANSIBLE_HOSTS=/etc/ansible/ec2.py
}

if [ -d /etc/ansible ]
then
dynamic
echo "Info::Dynamic Inventory file has been deployed"
else
sudo mkdir /etc/ansible
dynamic
echo "Info::Dynamic Inventory file has been deployed"
fi

#Change the default configuratio in ansible.cfg
inventory = /etc/ansible/ec2.py

#To manage the inventory output (update ec2.ini file)
group_by_tag_keys = True

# To allow ssh from control node to managed node
cat > israrul.pem
++++++++++++++++
paste private key
++++++++++++++++

#Using an SSH agent is the best way to authenticate with your end nodes, as this alleviates the need to copy your .pem files around. To add an agent, do
ssh-agent bash
ssh-add haque65.pem


/etc/ansible/ec2.py --refresh-cache

/etc/ansible/ec2.py --list |grep ec2_id

ansible -u ec2-user i-07dbb375486f717111 -m ping   #Defining user is important bcz key is related to ec2-user

#To execute the playbook using dynamic inventory
ansible-playbook -i /etc/ansible/ec2.py -s -u ec2-user -l i-07dbb375486f71123 pp1.yaml
ansible-playbook -i ec2.py --limit tag_Ansible_managed roles/pp1.yaml

#Run a module using tags on dynamic inventory's host
ansible -i ec2.py -u centos "tag_Ansible_managed" -m ping
ansible -i ec2.py -u centos -b "tag_Ansible_managed" -m yum -a "name=httpd state=absent"

##Additional Resource
https://medium.com/@dhoeric/ansible-dynamic-inventory-with-aws-ec2-80d075fcf430
