#!/bin/bash -xe

df -Ph > .df_out
for use in `df -h|awk -F ' ' '{print $5}' | grep -v ^Use | cut -d% -f1`
do
if [ $use -gt 20 ]
then
mount=`grep -w $use .df_out| awk -F" " '{print $6}'`
echo "Warning::Utilization of $mount is greater than 20."
fi
done
rm -rf .df_out